


// Grab container info
const container_div = document.querySelector('.container');
const gridSizeInfo_p = document.getElementById('grid-info');

// Grab user defined dimensions of the grid
const userRow_input = document.getElementById('grid-rows');
const userCol_input = document.getElementById('grid-cols');


// Grab buttons
const resize_btn = document.getElementById('resize-btn');
const clear_btn = document.getElementById('clear-btn');
const randomize_btn = document.getElementById('randomize-btn');

// Grab color picker canvas
const colorWheel_canvas = document.getElementById('color-wheel');

// Grid Dimensions obj
let gridProps = {
    height:     16,
    width:      16,
    colorOption:        'standard',
    boxColor:           'rgb(0, 0, 0)'
} 

// Color picker dimensions
const colorPicker = {
    x: 100,
    y: 100,
    width: 7,
    height: 7
}

const resizeButton = () => {

    resize_btn.addEventListener('click', () => {
        if (userRow_input.valueAsNumber === 0 || userRow_input.valueAsNumber > 128 || userCol_input.valueAsNumber === 0 || userCol_input.valueAsNumber > 128) {
            console.log("Grid Dimensions has to be greater than 0 and less than 128");
            return;
        }
        console.log(userCol_input.valueAsNumber + " " + userRow_input.valueAsNumber)
        gridProps.width = userRow_input.valueAsNumber;
        gridProps.height = userCol_input.valueAsNumber;
        clearContainer();
        appendGridToContainer(create2DGrid(gridProps.width, gridProps.height));
    });
}

const style2DGrid = (div2DGrid,numOfRows,numOfCols) => {
    
    let uniqueID = 0;
    for (let i=0; i < numOfRows; ++i) {
        div2DGrid[i].style = `width: 100%; height: ${container_div.clientHeight/numOfCols}%; display: flex; flexDirection: row;`;
        for (let j=0; j < numOfCols; ++j) {
            div2DGrid[i][j].id = `box-${uniqueID}`;
            uniqueID += 1;
            div2DGrid[i][j].style = `width: ${container_div.clientWidth/numOfRows}%; height: 100%;`;

            // add sketchability
            div2DGrid[i][j].addEventListener('mouseover',() => {
                if (gridProps.colorOption === 'rainbow') {
                    div2DGrid[i][j].style.backgroundColor = `rgb(${Math.floor(Math.random() * 255)}, ${Math.floor(Math.random() * 255)}, ${Math.floor(Math.random() * 255)})`;
                }
                else {
                    div2DGrid[i][j].style.backgroundColor = gridProps.boxColor;
                }
            });
        }
    }

    console.log(div2DGrid)
    return div2DGrid;
}


const create2DGrid = (rows,columns) => {

    let div2DArray = [[]];
    // Create the 2D grid
    for (let i=0; i < rows; ++i) {
        div2DArray[i] = document.createElement('div'); // Creating the rows
        for (let j=0; j < columns; ++j) {
            div2DArray[i][j] = document.createElement('div'); // Creating the columns
            div2DArray[i].appendChild(div2DArray[i][j]); 
        }
    }

    // add styles before returning the 2Dgrid
    style2DGrid(div2DArray, rows, columns);

    // Display the size in the UI
    gridSizeInfo_p.innerText = `${rows}x${columns} Grid`

    return div2DArray;
}

const appendGridToContainer = (div2DArray) => {

    for (let i=0; i < div2DArray.length; ++i) {
        container_div.appendChild(div2DArray[i]);
    }
}

const clearContainer = () => {
    container_div.querySelectorAll('div').forEach((child) => child.remove());
}

const clearButton = () => {
    clear_btn.addEventListener('click', () => {
        gridProps.colorOption = 'standard';
        gridProps.boxColor = 'rgb(0, 0, 0)';
        container_div.querySelectorAll('div').forEach((child) => child.style.backgroundColor = '#FFF');
    });
}

const randomizeButton = () => {

    randomize_btn.addEventListener('click', () => {
        gridProps.colorOption = 'rainbow';
    });
}

/**
    @params 
        hue in range[0,360]
        saturation, value range in [0, 1]
    @returns
        [r, g, b] each in range [0, 255]
 */
const hsv2rgb = (hue, saturation, value) => {
    let chroma = saturation * value;
    let hue1 = hue / 60;
    let x = chroma * (1 - Math.abs((hue1 % 2)-1));
    let r1,g1,b1;
    
    if (hue1 >= 0 && hue1 <= 1) {
        ([r1, g1, b1] = [chroma, x, 0]);

    } else if (hue1 >= 1 && hue1 <= 2) {
        ([r1, g1, b1] = [x, chroma, 0]);

    } else if (hue1 >= 2 && hue1 <= 3) {
        ([r1, g1, b1] = [0, chroma, x]);

    } else if (hue1 >= 3 && hue1 <= 4) {
        ([r1, g1, b1] = [0, x, chroma]);

    } else if (hue1 >= 4 && hue1 <= 5) {
        ([r1, g1, b1] = [x, 0, chroma]);

    } else if (hue1 >= 5 && hue1 <= 6) {
        ([r1, g1, b1] = [chroma, 0, x]);
    }

    let m = value - chroma;
    let [r, g, b] = [r1+m, g1+m, b1+m];

    // convert from [0..1] to [0,255]
    return [r*255, g*255, b*255];
}

const xy2polar = (x, y) => {
    let r = Math.sqrt(x*x + y*y);
    let phi = Math.atan2(y, x);
    return [r, phi];
  }
  
  // rad in [-π, π] range
  // return degree in [0, 360] range
const rad2deg = (rad) => {
    return ((rad + Math.PI) / (2 * Math.PI)) * 360;
  }

const colorWheelSetup = () => {
    
    // Get canvas context
    let cntx = colorWheel_canvas.getContext("2d"); 
    let radius = 50;
    let colorSpace = cntx.createImageData(2*radius, 2*radius);
    let imgData = colorSpace.data;

    // pixel calculations inside circle
    let distance = 0; // this will store the euclidian distance of the (x, y) point in the circle
    let pixelWidth = 4; // Each pixel needs 4 slots to represent RGBa
    let rowLength = 2*radius;
    let adjustedX = 0; // transform x from [-50, 50] to [0, 100] ( to map to image data array)
    let adjustedY = 0; // transfrom y from [-50, 50] to [0, 100] ( to map to image data array)  
    let pointIndex = 0; // Pixel calculated index
    let r, phi, deg = 0;
    let hue, saturation, value, alpha = 0;
    let [red, green, blue] = 0;

    // Go around the circle
    for (let x=-radius; x < radius; ++x) {
        for (let y=-radius; y < radius; ++y) {
            [r, phi] = xy2polar(x, y);

            if (distance > radius) {
                continue // Skip all the points outside the circle
            }

            deg = rad2deg(phi);
            // Figure out the starting index of this pixel in the image data array.
            adjustedX = x + radius; // transform x from [-50, 50] to [0, 100] ( to map to image data array)
            adjustedY = y + radius; // transfrom y from [-50, 50] to [0, 100] ( to map to image data array)  
            pointIndex = (adjustedX + (adjustedY * rowLength)) + pixelWidth; // Get the index of the point

            hue = deg;
            saturation = r / radius;
            value = 1.0;
            [red, green, blue] = hsv2rgb(hue, saturation, value);
            alpha = 255;

            imgData[index] = red;
            imgData[index + 1] = green;
            imgData[index + 2] = blue;
            imgData[index + 3] = alpha;
        }
    }
    
    cntx.putImageData(colorSpace, 0, 0);

}


const initGrid = () => {

    // Define Button functionality
    clearButton();
    resizeButton();
    randomizeButton();


    const grid = create2DGrid(gridProps.width, gridProps.height);
    appendGridToContainer(grid);
}


// Start The Sketch
initGrid();


